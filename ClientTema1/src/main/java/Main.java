import proto.Person;
import proto.PersonServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        PersonServiceGrpc.PersonServiceBlockingStub personStub = PersonServiceGrpc.newBlockingStub(channel);
        String ch;
        boolean isRunning = true;
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Your name is: ");
            String name = sc.next();
            System.out.println("Your CNP is: ");
            String cnp = sc.next();

            System.out.println(personStub.getInformation(Person.PersonRequest.newBuilder().setName(name)
                    .setCnp(cnp).build()));
            System.out.println("Do you want to continue? (y/n)");
            ch = sc.next();
        } while (isRunning && ch.equals("y"));
        channel.shutdown();
    }

}
