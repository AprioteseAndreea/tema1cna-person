package service;

import io.grpc.stub.StreamObserver;
import proto.Person;
import proto.PersonServiceGrpc;

import java.time.LocalDate;
import java.time.Period;

public class PersonImpl extends PersonServiceGrpc.PersonServiceImplBase {

    public static int getYear(String cnp) {
        String year = null;
        switch (cnp.charAt(0)) {
            case '1':
            case '2':
            case '7':
            case '8': {
                year = "19";
                break;
            }
            case '3':
            case '4': {
                year = "18";
                break;
            }
            case '5':
            case '6': {
                year = "20";
                break;
            }
        }
        StringBuilder y = new StringBuilder();
        y.append(year + cnp.substring(1, 3));
        int yearInt = Integer.parseInt(String.valueOf(y));
        return yearInt;

    }

    public static int getDayOrMonth(String month) {
        int monthInt = 0;
        if (month.substring(0, 1).equals("0")) {
            monthInt = Integer.parseInt(month.substring(1, 2));
        } else {
            monthInt = Integer.parseInt(month.substring(0, 2));
        }
        return monthInt;
    }

    public static String calculateAge(String cnp) {

        String cnpMonth = cnp.substring(3, 5);
        String cnpDay = cnp.substring(5, 7);

        LocalDate l = LocalDate.of(getYear(cnp), getDayOrMonth(cnpMonth), getDayOrMonth(cnpDay));
        LocalDate now = LocalDate.now();

        Period diff = Period.between(l, now);

        StringBuilder y = new StringBuilder();
        y.append(diff.getYears() + " " + diff.getMonths() + " " + diff.getDays());
        String age = String.valueOf(y);

        return age;

    }

    public static String calculateGender(String cnp) {

        String gender = null;
        switch (cnp.charAt(0)) {
            case '1':
            case '3':
            case '5':
            case '7': {
                gender = "M";
                break;
            }
            case '2':
            case '4':
            case '6':
            case '8': {
                gender = "F";
                break;
            }
        }
        return gender;

    }

    @Override
    public void getInformation(Person.PersonRequest request, StreamObserver<Person.PersonResponse> responseObserver) {
        Person.PersonResponse response = Person.PersonResponse.newBuilder().setName(request.getName()).
                setGender(calculateGender(request.getCnp())).setAge(calculateAge(request.getCnp())).build();

            System.out.println("Name: " + request.getName() + ", gender: " + response.getGender() +
                    ", age: " + response.getAge());

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }


}
